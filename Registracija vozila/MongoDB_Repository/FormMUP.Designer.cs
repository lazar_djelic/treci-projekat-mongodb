﻿namespace MongoDB_Repository
{
    partial class FormMUP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTPregled = new MetroFramework.Controls.MetroButton();
            this.btnObrisi = new MetroFramework.Controls.MetroButton();
            this.dgvRezultat = new System.Windows.Forms.DataGridView();
            this.lblObavestenje = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRezultat)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTPregled
            // 
            this.btnTPregled.Location = new System.Drawing.Point(23, 79);
            this.btnTPregled.Name = "btnTPregled";
            this.btnTPregled.Size = new System.Drawing.Size(186, 30);
            this.btnTPregled.TabIndex = 0;
            this.btnTPregled.Text = "Upravljanje tehnickim pregledima";
            this.btnTPregled.UseSelectable = true;
            this.btnTPregled.Click += new System.EventHandler(this.btnTPregled_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(418, 79);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(112, 30);
            this.btnObrisi.TabIndex = 1;
            this.btnObrisi.Text = "Brisanje svih servisa";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // dgvRezultat
            // 
            this.dgvRezultat.AllowUserToAddRows = false;
            this.dgvRezultat.AllowUserToDeleteRows = false;
            this.dgvRezultat.AllowUserToResizeRows = false;
            this.dgvRezultat.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvRezultat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRezultat.Location = new System.Drawing.Point(23, 141);
            this.dgvRezultat.MultiSelect = false;
            this.dgvRezultat.Name = "dgvRezultat";
            this.dgvRezultat.ReadOnly = true;
            this.dgvRezultat.Size = new System.Drawing.Size(507, 228);
            this.dgvRezultat.TabIndex = 3;
            // 
            // lblObavestenje
            // 
            this.lblObavestenje.AutoSize = true;
            this.lblObavestenje.Location = new System.Drawing.Point(23, 119);
            this.lblObavestenje.Name = "lblObavestenje";
            this.lblObavestenje.Size = new System.Drawing.Size(0, 0);
            this.lblObavestenje.TabIndex = 2;
            // 
            // FormMUP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 392);
            this.Controls.Add(this.lblObavestenje);
            this.Controls.Add(this.dgvRezultat);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.btnTPregled);
            this.Name = "FormMUP";
            this.Text = "Uprava saobracajne policije";
            this.Load += new System.EventHandler(this.FormMUP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRezultat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnTPregled;
        private MetroFramework.Controls.MetroButton btnObrisi;
        private System.Windows.Forms.DataGridView dgvRezultat;
        private MetroFramework.Controls.MetroLabel lblObavestenje;
    }
}