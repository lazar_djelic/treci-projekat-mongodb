﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace MongoDB_Repository
{
    class RadSaBazom
    {
        public bool dodajServis(string nNaziv, string nMesto, string nAdresa)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            ServisTehnickogPregleda stp = new ServisTehnickogPregleda
            {
                Naziv = nNaziv,
                Mesto = nMesto,
                Adresa = nAdresa
            };

            if (collection.Insert(stp).Ok)
                return true;
            else
                return false;
        }

        public bool izmeniServis(ObjectId id, string nNaziv, string nMesto, string nAdresa)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var servisiCollection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            var query = Query.EQ("_id", id);

            var update = MongoDB.Driver.Builders.Update.Set("Naziv", BsonValue.Create(nNaziv))
                .Set("Mesto", BsonValue.Create(nMesto))
                .Set("Adresa", BsonValue.Create(nAdresa));

            if (servisiCollection.Update(query, update).Ok)
                return true;
            else
                return false;
        }

        public bool obrisiServis(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var servisiCollection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            var query = Query.EQ("_id", id);

            if (servisiCollection.Remove(query).Ok)
                return true;
            else
                return false;
            
        }

        public bool obrisiSveServise()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var servisiCollection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            if (servisiCollection.RemoveAll().Ok)
                return true;
            else
                return false;
        }

        public MongoCollection<ServisTehnickogPregleda> vratiSveServise()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var servisiCollection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            return servisiCollection;
        }

        public ServisTehnickogPregleda pronadjiServisPoIDu(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var servisiCollection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            var query = Query.EQ("_id", id);

            ServisTehnickogPregleda s = servisiCollection.Find(query).First();

            return s;
        }
    }
}
