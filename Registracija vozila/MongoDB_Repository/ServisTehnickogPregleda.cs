﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MongoDB_Repository
{
    class ServisTehnickogPregleda
    {
        public ObjectId Id { get; set; }
        public String Naziv { get; set; }
        public String Mesto { get; set; }
        public String Adresa { get; set; }
    }
}
