﻿namespace MongoDB_Repository
{
    partial class FormTehnickiPregledVozila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.txtNazivServisa = new MetroFramework.Controls.MetroTextBox();
            this.lblBrisanjeID = new MetroFramework.Controls.MetroLabel();
            this.btnObrisi = new MetroFramework.Controls.MetroButton();
            this.txtBrisanjeID = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtMesto = new MetroFramework.Controls.MetroTextBox();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(233, 246);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 37);
            this.btnDodaj.TabIndex = 9;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // txtNazivServisa
            // 
            // 
            // 
            // 
            this.txtNazivServisa.CustomButton.Image = null;
            this.txtNazivServisa.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtNazivServisa.CustomButton.Name = "";
            this.txtNazivServisa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNazivServisa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNazivServisa.CustomButton.TabIndex = 1;
            this.txtNazivServisa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNazivServisa.CustomButton.UseSelectable = true;
            this.txtNazivServisa.CustomButton.Visible = false;
            this.txtNazivServisa.Lines = new string[0];
            this.txtNazivServisa.Location = new System.Drawing.Point(125, 100);
            this.txtNazivServisa.MaxLength = 32767;
            this.txtNazivServisa.Name = "txtNazivServisa";
            this.txtNazivServisa.PasswordChar = '\0';
            this.txtNazivServisa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNazivServisa.SelectedText = "";
            this.txtNazivServisa.SelectionLength = 0;
            this.txtNazivServisa.SelectionStart = 0;
            this.txtNazivServisa.ShortcutsEnabled = true;
            this.txtNazivServisa.Size = new System.Drawing.Size(184, 23);
            this.txtNazivServisa.TabIndex = 1;
            this.txtNazivServisa.UseSelectable = true;
            this.txtNazivServisa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNazivServisa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblBrisanjeID
            // 
            this.lblBrisanjeID.AutoSize = true;
            this.lblBrisanjeID.Location = new System.Drawing.Point(52, 190);
            this.lblBrisanjeID.Name = "lblBrisanjeID";
            this.lblBrisanjeID.Size = new System.Drawing.Size(67, 19);
            this.lblBrisanjeID.TabIndex = 6;
            this.lblBrisanjeID.Text = "ID servisa:";
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(82, 246);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(75, 37);
            this.btnObrisi.TabIndex = 8;
            this.btnObrisi.Text = "Obrisi";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // txtBrisanjeID
            // 
            // 
            // 
            // 
            this.txtBrisanjeID.CustomButton.Image = null;
            this.txtBrisanjeID.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtBrisanjeID.CustomButton.Name = "";
            this.txtBrisanjeID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBrisanjeID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrisanjeID.CustomButton.TabIndex = 1;
            this.txtBrisanjeID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrisanjeID.CustomButton.UseSelectable = true;
            this.txtBrisanjeID.CustomButton.Visible = false;
            this.txtBrisanjeID.Lines = new string[0];
            this.txtBrisanjeID.Location = new System.Drawing.Point(125, 190);
            this.txtBrisanjeID.MaxLength = 32767;
            this.txtBrisanjeID.Name = "txtBrisanjeID";
            this.txtBrisanjeID.PasswordChar = '\0';
            this.txtBrisanjeID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrisanjeID.SelectedText = "";
            this.txtBrisanjeID.SelectionLength = 0;
            this.txtBrisanjeID.SelectionStart = 0;
            this.txtBrisanjeID.ShortcutsEnabled = true;
            this.txtBrisanjeID.Size = new System.Drawing.Size(184, 23);
            this.txtBrisanjeID.TabIndex = 7;
            this.txtBrisanjeID.UseSelectable = true;
            this.txtBrisanjeID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrisanjeID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(14, 85);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(105, 38);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Naziv servisa za\r\ntehnicki pregled:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(71, 132);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(48, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Mesto:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(66, 161);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(53, 19);
            this.metroLabel6.TabIndex = 4;
            this.metroLabel6.Text = "Adresa:";
            // 
            // txtMesto
            // 
            // 
            // 
            // 
            this.txtMesto.CustomButton.Image = null;
            this.txtMesto.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtMesto.CustomButton.Name = "";
            this.txtMesto.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtMesto.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtMesto.CustomButton.TabIndex = 1;
            this.txtMesto.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtMesto.CustomButton.UseSelectable = true;
            this.txtMesto.CustomButton.Visible = false;
            this.txtMesto.Lines = new string[0];
            this.txtMesto.Location = new System.Drawing.Point(125, 132);
            this.txtMesto.MaxLength = 32767;
            this.txtMesto.Name = "txtMesto";
            this.txtMesto.PasswordChar = '\0';
            this.txtMesto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMesto.SelectedText = "";
            this.txtMesto.SelectionLength = 0;
            this.txtMesto.SelectionStart = 0;
            this.txtMesto.ShortcutsEnabled = true;
            this.txtMesto.Size = new System.Drawing.Size(184, 23);
            this.txtMesto.TabIndex = 3;
            this.txtMesto.UseSelectable = true;
            this.txtMesto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtMesto.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(125, 161);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(184, 23);
            this.txtAdresa.TabIndex = 5;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // FormTehnickiPregledVozila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 318);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtMesto);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.lblBrisanjeID);
            this.Controls.Add(this.txtBrisanjeID);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.txtNazivServisa);
            this.Name = "FormTehnickiPregledVozila";
            this.Text = "Tehnicki pregled vozila";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTehnickiPregledVozila_FormClosing);
            this.Load += new System.EventHandler(this.TehnickiPregled_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroTextBox txtNazivServisa;
        private MetroFramework.Controls.MetroLabel lblBrisanjeID;
        private MetroFramework.Controls.MetroButton btnObrisi;
        private MetroFramework.Controls.MetroTextBox txtBrisanjeID;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtMesto;
        private MetroFramework.Controls.MetroTextBox txtAdresa;
    }
}