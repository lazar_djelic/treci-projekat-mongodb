﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MongoDB_Repository
{
    public partial class FormTehnickiPregledVozila : MetroFramework.Forms.MetroForm
    {
        RadSaBazom rsb;
        ObjectId IDzaModifikaciju;
        ServisTehnickogPregleda servisZaModifikaciju;

        public FormTehnickiPregledVozila()
        {
            InitializeComponent();
        }

        public FormTehnickiPregledVozila(ObjectId id)
        {
            InitializeComponent();
            IDzaModifikaciju = id;
        }

        private void TehnickiPregled_Load(object sender, EventArgs e)
        {
            rsb = new RadSaBazom();
            if (IDzaModifikaciju != ObjectId.Empty)
            {
                servisZaModifikaciju = rsb.pronadjiServisPoIDu(IDzaModifikaciju);
                txtNazivServisa.Text = servisZaModifikaciju.Naziv;
                txtMesto.Text = servisZaModifikaciju.Mesto;
                txtAdresa.Text = servisZaModifikaciju.Adresa;

                txtBrisanjeID.Text = IDzaModifikaciju.ToString();

                btnDodaj.Text = "Izmeni";
            }
            else
            {
                lblBrisanjeID.Visible = false;
                txtBrisanjeID.Visible = false;
                btnObrisi.Visible = false;
            }
        }

        public void ocistiPolja()
        {
            txtNazivServisa.Text = "";
            txtMesto.Text = "";
            txtAdresa.Text = "";
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (txtNazivServisa.Text != "" && txtMesto.Text != "" && txtAdresa.Text != "")
            {
                if(IDzaModifikaciju == ObjectId.Empty)
                {
                    if (rsb.dodajServis(txtNazivServisa.Text, txtMesto.Text, txtAdresa.Text))
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno dodat servis!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ocistiPolja();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (rsb.izmeniServis(IDzaModifikaciju, txtNazivServisa.Text, txtMesto.Text, txtAdresa.Text))
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno izmenjen servis!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MetroFramework.MetroMessageBox.Show(this, "Sva polja moraju biti popunjena!", "",
                      MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (txtBrisanjeID.Text != "")
            {
                var id = ObjectId.Parse(txtBrisanjeID.Text);

                if (rsb.obrisiServis(id))
                {
                    MetroFramework.MetroMessageBox.Show(this, 
                        "Servis tehnickog pregleda je uspesno obrisan!",
                                "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                    MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MetroFramework.MetroMessageBox.Show(this, "Morate uneti ID servisa!", "",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void FormTehnickiPregledVozila_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}


