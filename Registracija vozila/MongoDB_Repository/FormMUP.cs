﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MongoDB_Repository
{
    public partial class FormMUP : MetroFramework.Forms.MetroForm
    {
        RadSaBazom rsb;

        public FormMUP()
        {
            InitializeComponent();
        }

        private void FormMUP_Load(object sender, EventArgs e)
        {
            rsb = new RadSaBazom();
            Prikazi();
        }

        private void Prikazi()
        {
            dgvRezultat.Rows.Clear();
            List<ServisTehnickogPregleda> lista = new List<ServisTehnickogPregleda>();
            var servisiCollection = rsb.vratiSveServise();

            if (servisiCollection.Count() == 0)
            {
                dgvRezultat.Visible = false;
                lblObavestenje.Text = "Trenutno nema ovlascenih servisa!";
            }
            else
            {
                dgvRezultat.Visible = true;
                lblObavestenje.Text = "";
                var source = new BindingSource();
                foreach (ServisTehnickogPregleda s in servisiCollection.FindAll())
                    lista.Add(s);
                source.DataSource = lista;
                dgvRezultat.DataSource = source;
                dgvRezultat.AutoResizeColumns();
            }
        }

        private void btnTPregled_Click(object sender, EventArgs e)
        {
            FormTehnickiPregledVozila tpv;
            if (dgvRezultat.SelectedRows.Count == 0)
            {
                tpv = new FormTehnickiPregledVozila();
                tpv.ShowDialog();
            }
            else
            {
                int izabranaStavka = dgvRezultat.SelectedRows[0].Index;
                ObjectId id = (ObjectId)dgvRezultat["Id", izabranaStavka].Value;
                tpv = new FormTehnickiPregledVozila(id);
                tpv.ShowDialog();
            }

            if (tpv.DialogResult == DialogResult.OK)
                Prikazi();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (rsb.obrisiSveServise())
            {
                Prikazi();
                MetroFramework.MetroMessageBox.Show(this, "Svi podaci su uspesno obrisani", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                     MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
