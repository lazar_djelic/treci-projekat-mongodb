﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public partial class FormServis : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;

        public FormServis(ServisTehnickogPregleda stp)
        {
            InitializeComponent();
            lblNaziv.Text = stp.Naziv;
            lblNaziv.Visible = true;
        }

        private void FormServis_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();
            PrikaziVozace();
            PrikaziVozila();
        }

        private void PrikaziVozace()
        {
            dgvVozaci.Rows.Clear();
            List<Vozac> listaVozaca = new List<Vozac>();
            var vozaciCollection = fb.vratiSveVozace();

            if (vozaciCollection.Count() == 0)
            {
                dgvVozaci.Visible = false;
                lblObavestenje1.Text = "Trenutno nema vozaca u bazi!";
            }
            else
            {
                dgvVozaci.Visible = true;
                lblObavestenje1.Text = "";
                var source = new BindingSource();
                foreach (Vozac v in vozaciCollection.FindAll())
                    listaVozaca.Add(v);
                source.DataSource = listaVozaca;
                dgvVozaci.DataSource = source;
                dgvVozaci.AutoResizeColumns();
            }
        }

        private void PrikaziVozila()
        {
            dgvVozila.Rows.Clear();
            List<Vozilo> listaVozila = new List<Vozilo>();
            var vozilaCollection = fb.vratiSvaVozila();

            if (vozilaCollection.Count() == 0)
            {
                dgvVozila.Visible = false;
                lblObavestenje2.Text = "Trenutno nema vozila u bazi!";
            }
            else
            {
                dgvVozila.Visible = true;
                lblObavestenje2.Text = "";
                var source = new BindingSource();
                foreach (Vozilo v in vozilaCollection.FindAll())
                    listaVozila.Add(v);
                source.DataSource = listaVozila;
                dgvVozila.DataSource = source;
                dgvVozila.AutoResizeColumns();
            }
        }

        private void btnRegistruj_Click(object sender, EventArgs e)
        {
            FormRegistracija fr;

            if (dgvVozila.SelectedRows.Count == 0)
            {
                fr = new FormRegistracija();
            }
            else
            {
                int izabranaStavka = dgvVozila.SelectedRows[0].Index;
                ObjectId id = (ObjectId)dgvVozila["Id", izabranaStavka].Value;
                fr = new FormRegistracija(id);
            }

            fr.ShowDialog();

            if (fr.DialogResult == DialogResult.OK)
                PrikaziVozila();
        }

        private void btnTPregled_Click(object sender, EventArgs e)
        {
            FormTehnickiPregled tp;

            if (dgvVozila.SelectedRows.Count == 0)
            {
                tp = new FormTehnickiPregled();
            }
            else
            {
                int izabranaStavka = dgvVozila.SelectedRows[0].Index;
                ObjectId id = (ObjectId)dgvVozila["Id", izabranaStavka].Value;
                tp = new FormTehnickiPregled(id);
            }

            tp.ShowDialog();

            if (tp.DialogResult == DialogResult.OK)
                PrikaziVozila();
        }

        private void btnVozilo_Click(object sender, EventArgs e)
        {
            FormVozilo fv = new FormVozilo();
            if (dgvVozila.SelectedRows.Count == 0)
            {
                fv = new FormVozilo();
            }
            else
            {
                int izabranaStavka = dgvVozila.SelectedRows[0].Index;
                ObjectId id = (ObjectId)dgvVozila["Id", izabranaStavka].Value;
                fv = new FormVozilo(true, id);
            }
            
            fv.ShowDialog();

            if (fv.DialogResult == DialogResult.OK)
                PrikaziVozila();
        }

        private void btnVozac_Click(object sender, EventArgs e)
        {
            FormVozac fv;
            if(dgvVozaci.SelectedRows.Count == 0)
            {
                fv = new FormVozac();
            }
            else
            {
                int izabranaStavka = dgvVozaci.SelectedRows[0].Index;
                ObjectId id = (ObjectId)dgvVozaci["Id", izabranaStavka].Value;
                fv = new FormVozac(true, id);
            }

            fv.ShowDialog();

            if (fv.DialogResult == DialogResult.OK)
                PrikaziVozace();
        }
    }
}
