﻿namespace MongoDB_Repository_Servis
{
    partial class FormTehnickiPregled
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtIDvozila = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.clbPregled = new System.Windows.Forms.CheckedListBox();
            this.btnPotvrdi = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(25, 81);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(107, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Unesite ID vozila:";
            // 
            // txtIDvozila
            // 
            // 
            // 
            // 
            this.txtIDvozila.CustomButton.Image = null;
            this.txtIDvozila.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtIDvozila.CustomButton.Name = "";
            this.txtIDvozila.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIDvozila.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIDvozila.CustomButton.TabIndex = 1;
            this.txtIDvozila.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIDvozila.CustomButton.UseSelectable = true;
            this.txtIDvozila.CustomButton.Visible = false;
            this.txtIDvozila.Lines = new string[0];
            this.txtIDvozila.Location = new System.Drawing.Point(138, 77);
            this.txtIDvozila.MaxLength = 32767;
            this.txtIDvozila.Name = "txtIDvozila";
            this.txtIDvozila.PasswordChar = '\0';
            this.txtIDvozila.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDvozila.SelectedText = "";
            this.txtIDvozila.SelectionLength = 0;
            this.txtIDvozila.SelectionStart = 0;
            this.txtIDvozila.ShortcutsEnabled = true;
            this.txtIDvozila.Size = new System.Drawing.Size(262, 23);
            this.txtIDvozila.TabIndex = 1;
            this.txtIDvozila.UseSelectable = true;
            this.txtIDvozila.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIDvozila.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(25, 127);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(188, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Kategorije tehnickog pregleda:";
            // 
            // clbPregled
            // 
            this.clbPregled.FormattingEnabled = true;
            this.clbPregled.Items.AddRange(new object[] {
            "Cisto vozilo",
            "Ostecenje vozila (do 5%)",
            "Ispravnost kocnica",
            "Ispravnost rucne kocnice",
            "Farovi, svetla za maglu i stop-svetla",
            "Izduvni gasovi",
            "Oprema u vozilu"});
            this.clbPregled.Location = new System.Drawing.Point(25, 149);
            this.clbPregled.Name = "clbPregled";
            this.clbPregled.Size = new System.Drawing.Size(375, 169);
            this.clbPregled.TabIndex = 3;
            // 
            // btnPotvrdi
            // 
            this.btnPotvrdi.Location = new System.Drawing.Point(276, 345);
            this.btnPotvrdi.Name = "btnPotvrdi";
            this.btnPotvrdi.Size = new System.Drawing.Size(124, 37);
            this.btnPotvrdi.TabIndex = 4;
            this.btnPotvrdi.Text = "Posalji podatke";
            this.btnPotvrdi.UseSelectable = true;
            this.btnPotvrdi.Click += new System.EventHandler(this.btnPotvrdi_Click);
            // 
            // FormTehnickiPregled
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 425);
            this.Controls.Add(this.btnPotvrdi);
            this.Controls.Add(this.clbPregled);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtIDvozila);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FormTehnickiPregled";
            this.Text = "Tehnicki pregled";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTehnickiPregled_FormClosing);
            this.Load += new System.EventHandler(this.FormTehnickiPregled_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtIDvozila;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.CheckedListBox clbPregled;
        private MetroFramework.Controls.MetroButton btnPotvrdi;
    }
}