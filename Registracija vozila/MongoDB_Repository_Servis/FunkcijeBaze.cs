﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace MongoDB_Repository_Servis
{
    class FunkcijeBaze
    {
        public ServisTehnickogPregleda loginServisa(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<ServisTehnickogPregleda>("servisi");

            var query = Query.EQ("_id", id);

            ServisTehnickogPregleda s = null;
            if (collection.Find(query).Count() != 0)
                s = collection.Find(query).First();

            return s;
        }

        public ObjectId dodajVozaca(string nIme, string nPrez, string nJMBG, string nBRL, string nAdresa, string nTel)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozac>("vozaci");

            Vozac v = new Vozac
            {
                Ime = nIme,
                Prezime = nPrez,
                JMBG = nJMBG,
                BrLicneKarte = nBRL,
                Adresa = nAdresa,
                Telefon = nTel
            };

            if (collection.Insert(v).Ok)
                return v.Id;
            else
                return ObjectId.Empty;
        }

        public bool izmeniVozaca(ObjectId id, string nIme, string nPrez, string nJMBG, string nBRL, string nAdresa, string nTel)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozac>("vozaci");

            var query = Query.EQ("_id", id);

            var update = MongoDB.Driver.Builders.Update.Set("Ime", BsonValue.Create(nIme))
                .Set("Prezime", BsonValue.Create(nPrez))
                .Set("JMBG", BsonValue.Create(nJMBG))
                .Set("BrLicneKarte", BsonValue.Create(nBRL))
                .Set("Adresa", BsonValue.Create(nAdresa))
                .Set("Telefon", BsonValue.Create(nTel));

            if (collection.Update(query, update).Ok)
                return true;
            else
                return false;
        }

        public bool obrisiVozaca(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozac>("vozaci");

            var query = Query.EQ("_id", id);

            if (collection.Remove(query).Ok)
                return true;
            else
                return false;
        }

        public Vozac vratiVozacaSaIDem(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozac>("vozaci");

            var query = Query.EQ("_id", id);

            Vozac v = new Vozac();
            if (collection.Find(query).Count() != 0)
                v = collection.Find(query).First();

            return v;
        }

        public MongoCollection<Vozac> vratiSveVozace()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozac>("vozaci");

            return collection;
        }

        public ObjectId dodajVozilo(ObjectId nIdV, string nModel, string nBoja, Int32 nGodiste, Int32 nSnaga, float nZap, int nVV, bool nReg, bool nOsig)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            Vozilo v = new Vozilo
            {
                IdVlasnika = nIdV,
                Model = nModel,
                Boja = nBoja,
                Godiste = nGodiste,
                SnagaKW = nSnaga,
                Zapremina = nZap,
                VrstaVozila = nVV,
                Registrovano = nReg,
                Osigurano = nOsig
            };

            if (collection.Insert(v).Ok)
                return v.Id;
            else
                return ObjectId.Empty;
        }

        public bool izmeniVozilo(ObjectId nId, ObjectId nIdV, string nModel, string nBoja, Int32 nGodiste, Int32 nSnaga, float nZap, int nVV, bool nReg, bool nOsig)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            var query = Query.EQ("_id", nId);

            var update = MongoDB.Driver.Builders.Update.Set("IdVlasnika", BsonValue.Create(nIdV))
                .Set("Model", BsonValue.Create(nModel))
                .Set("Boja", BsonValue.Create(nBoja))
                .Set("Godiste", BsonValue.Create(nGodiste))
                .Set("SnagaKW", BsonValue.Create(nSnaga))
                .Set("Zapremina", BsonValue.Create(nZap))
                .Set("VrstaVozila", BsonValue.Create(nVV))
                .Set("Registrovano", BsonValue.Create(nReg))
                .Set("Osigurano", BsonValue.Create(nOsig));

            if (collection.Update(query, update).Ok)
                return true;
            else
                return false;
        }

        public bool obrisiVozilo(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            var query = Query.EQ("_id", id);

            if (collection.Remove(query).Ok)
                return true;
            else
                return false;
        }

        public Vozilo vratiVoziloSaIDem(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            var query = Query.EQ("_id", id);

            Vozilo v = new Vozilo();
            if (collection.Find(query).Count() != 0)
                v = collection.Find(query).First();

            return v;
        }

        public MongoCollection<Vozilo> vratiSvaVozila()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            return collection;
        }

        public bool registrujVozilo(ObjectId id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<TehnickiPregled>("tehnickipregled");
            var query = Query.EQ("IdVozila", id);

            TehnickiPregled poslednji = null;
            foreach (TehnickiPregled t in collection.Find(query))
            {
                if (poslednji == null)
                {
                    poslednji = t;
                    continue;
                }
                DateTime dtp = DateTime.Parse(poslednji.Datum.ToString());
                DateTime dtt = DateTime.Parse(t.Datum.ToString());
                if (dtp < dtt)
                    poslednji = t;
            }

            if (poslednji == null)
                return false;

            DateTime dtp1 = DateTime.Parse(poslednji.Datum.ToString());
            if (dtp1 < DateTime.Now.AddDays(-62))
                return false;

            if (poslednji.IspunjenoUslova != 7)
                return false;

            Vozilo vozilo = vratiVoziloSaIDem(id);

            if (vozilo == null)
                return false;
            
            if (regVozilo(id))
                return true;
            else
                return false;
        }

        public bool regVozilo(ObjectId nId)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<Vozilo>("vozila");

            var query = Query.EQ("_id", nId);

            var update = MongoDB.Driver.Builders.Update.Set("Registrovano", BsonValue.Create(true));

            if (collection.Update(query, update).Ok)
                return true;
            else
                return false;
        }

        public bool dodajTehnickiPregled(TehnickiPregled t_Pregled)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("registracija");

            var collection = db.GetCollection<TehnickiPregled>("tehnickipregled");
            if (collection.Insert(t_Pregled).Ok)
                return true;
            else
                return false;
        }
    }
}
