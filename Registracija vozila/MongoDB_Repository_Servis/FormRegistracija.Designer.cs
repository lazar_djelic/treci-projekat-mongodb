﻿namespace MongoDB_Repository_Servis
{
    partial class FormRegistracija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtIDvozila = new MetroFramework.Controls.MetroTextBox();
            this.btnRegistruj = new MetroFramework.Controls.MetroButton();
            this.lblUspesnost = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 70);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(183, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Unesite jedinstveni broj vozila:";
            // 
            // txtIDvozila
            // 
            // 
            // 
            // 
            this.txtIDvozila.CustomButton.Image = null;
            this.txtIDvozila.CustomButton.Location = new System.Drawing.Point(266, 1);
            this.txtIDvozila.CustomButton.Name = "";
            this.txtIDvozila.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIDvozila.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIDvozila.CustomButton.TabIndex = 1;
            this.txtIDvozila.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIDvozila.CustomButton.UseSelectable = true;
            this.txtIDvozila.CustomButton.Visible = false;
            this.txtIDvozila.Lines = new string[0];
            this.txtIDvozila.Location = new System.Drawing.Point(23, 104);
            this.txtIDvozila.MaxLength = 32767;
            this.txtIDvozila.Name = "txtIDvozila";
            this.txtIDvozila.PasswordChar = '\0';
            this.txtIDvozila.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDvozila.SelectedText = "";
            this.txtIDvozila.SelectionLength = 0;
            this.txtIDvozila.SelectionStart = 0;
            this.txtIDvozila.ShortcutsEnabled = true;
            this.txtIDvozila.Size = new System.Drawing.Size(288, 23);
            this.txtIDvozila.TabIndex = 2;
            this.txtIDvozila.UseSelectable = true;
            this.txtIDvozila.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIDvozila.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnRegistruj
            // 
            this.btnRegistruj.Location = new System.Drawing.Point(23, 161);
            this.btnRegistruj.Name = "btnRegistruj";
            this.btnRegistruj.Size = new System.Drawing.Size(140, 45);
            this.btnRegistruj.TabIndex = 3;
            this.btnRegistruj.Text = "Registruj vozilo";
            this.btnRegistruj.UseSelectable = true;
            this.btnRegistruj.Click += new System.EventHandler(this.btnRegistruj_Click);
            // 
            // lblUspesnost
            // 
            this.lblUspesnost.AutoSize = true;
            this.lblUspesnost.Location = new System.Drawing.Point(208, 173);
            this.lblUspesnost.Name = "lblUspesnost";
            this.lblUspesnost.Size = new System.Drawing.Size(67, 19);
            this.lblUspesnost.TabIndex = 4;
            this.lblUspesnost.Text = "Uspesnost";
            // 
            // metroLabel2
            // 
            this.metroLabel2.ForeColor = System.Drawing.Color.Red;
            this.metroLabel2.Location = new System.Drawing.Point(23, 236);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(355, 67);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "Napomena:\r\nRegistraciju vozila je moguce obaviti u roku od 2 meseca\r\nnakon uspesn" +
    "o zavrsenog tehnickog pregleda!";
            // 
            // FormRegistracija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 332);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.lblUspesnost);
            this.Controls.Add(this.btnRegistruj);
            this.Controls.Add(this.txtIDvozila);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FormRegistracija";
            this.Text = "Registracija";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRegistracija_FormClosing);
            this.Load += new System.EventHandler(this.FormRegistracija_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtIDvozila;
        private MetroFramework.Controls.MetroButton btnRegistruj;
        private MetroFramework.Controls.MetroLabel lblUspesnost;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}