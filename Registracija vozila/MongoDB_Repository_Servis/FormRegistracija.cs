﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public partial class FormRegistracija : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;
        ObjectId idZaReg;

        public FormRegistracija()
        {
            InitializeComponent();
        }

        public FormRegistracija(ObjectId id)
        {
            idZaReg = id;
            InitializeComponent();
        }

        private void FormRegistracija_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();

            if (idZaReg != ObjectId.Empty)
                txtIDvozila.Text = idZaReg.ToString();
        }

        private void btnRegistruj_Click(object sender, EventArgs e)
        {
            String idVozila = txtIDvozila.Text;
            if (idVozila == String.Empty)
            {
                MetroFramework.MetroMessageBox.Show(this, "Morate uneti ID!", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if(fb.registrujVozilo(ObjectId.Parse(idVozila)))
            {
                MetroFramework.MetroMessageBox.Show(this, "Uspesno registrovano vozilo!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUspesnost.Text = "Uspešno registrovano";
                lblUspesnost.ForeColor = Color.Green;
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Doslo je do greške! Prvoverite uslove za registraciju i proverite da li ste uneli korektan ID.", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                lblUspesnost.Text = "Nije uspešno registrovano";
                lblUspesnost.ForeColor = Color.Red;
            }
        }

        private void FormRegistracija_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
