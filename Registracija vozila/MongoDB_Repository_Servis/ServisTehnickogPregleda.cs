﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB_Repository_Servis
{
    public class ServisTehnickogPregleda
    {
        public ObjectId Id { get; set; }
        public String Naziv { get; set; }
        public String Mesto { get; set; }
        public String Adresa { get; set; }
    }
}
