﻿namespace MongoDB_Repository_Servis
{
    partial class FormVozilo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbVrstaVozila = new System.Windows.Forms.ComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtZapremina = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtSnagaKW = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtGodiste = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtBoja = new MetroFramework.Controls.MetroTextBox();
            this.txtModel = new MetroFramework.Controls.MetroTextBox();
            this.txtID = new MetroFramework.Controls.MetroTextBox();
            this.txtIDVozila = new MetroFramework.Controls.MetroTextBox();
            this.lblID = new MetroFramework.Controls.MetroLabel();
            this.btnObrisi = new MetroFramework.Controls.MetroButton();
            this.cbReg = new MetroFramework.Controls.MetroCheckBox();
            this.cbOsig = new MetroFramework.Controls.MetroCheckBox();
            this.SuspendLayout();
            // 
            // cmbVrstaVozila
            // 
            this.cmbVrstaVozila.FormattingEnabled = true;
            this.cmbVrstaVozila.Items.AddRange(new object[] {
            "Putnicko",
            "Motocikl",
            "Teretno",
            "Autobus"});
            this.cmbVrstaVozila.Location = new System.Drawing.Point(160, 305);
            this.cmbVrstaVozila.Name = "cmbVrstaVozila";
            this.cmbVrstaVozila.Size = new System.Drawing.Size(146, 21);
            this.cmbVrstaVozila.TabIndex = 15;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(75, 308);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(77, 19);
            this.metroLabel7.TabIndex = 14;
            this.metroLabel7.Text = "Vrsta vozila:";
            // 
            // txtZapremina
            // 
            // 
            // 
            // 
            this.txtZapremina.CustomButton.Image = null;
            this.txtZapremina.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtZapremina.CustomButton.Name = "";
            this.txtZapremina.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtZapremina.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtZapremina.CustomButton.TabIndex = 1;
            this.txtZapremina.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtZapremina.CustomButton.UseSelectable = true;
            this.txtZapremina.CustomButton.Visible = false;
            this.txtZapremina.Lines = new string[0];
            this.txtZapremina.Location = new System.Drawing.Point(159, 274);
            this.txtZapremina.MaxLength = 32767;
            this.txtZapremina.Name = "txtZapremina";
            this.txtZapremina.PasswordChar = '\0';
            this.txtZapremina.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtZapremina.SelectedText = "";
            this.txtZapremina.SelectionLength = 0;
            this.txtZapremina.SelectionStart = 0;
            this.txtZapremina.ShortcutsEnabled = true;
            this.txtZapremina.Size = new System.Drawing.Size(147, 23);
            this.txtZapremina.TabIndex = 13;
            this.txtZapremina.UseSelectable = true;
            this.txtZapremina.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtZapremina.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(29, 278);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(124, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Zapremina motora:";
            // 
            // txtSnagaKW
            // 
            // 
            // 
            // 
            this.txtSnagaKW.CustomButton.Image = null;
            this.txtSnagaKW.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtSnagaKW.CustomButton.Name = "";
            this.txtSnagaKW.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSnagaKW.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSnagaKW.CustomButton.TabIndex = 1;
            this.txtSnagaKW.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSnagaKW.CustomButton.UseSelectable = true;
            this.txtSnagaKW.CustomButton.Visible = false;
            this.txtSnagaKW.Lines = new string[0];
            this.txtSnagaKW.Location = new System.Drawing.Point(159, 245);
            this.txtSnagaKW.MaxLength = 32767;
            this.txtSnagaKW.Name = "txtSnagaKW";
            this.txtSnagaKW.PasswordChar = '\0';
            this.txtSnagaKW.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSnagaKW.SelectedText = "";
            this.txtSnagaKW.SelectionLength = 0;
            this.txtSnagaKW.SelectionStart = 0;
            this.txtSnagaKW.ShortcutsEnabled = true;
            this.txtSnagaKW.Size = new System.Drawing.Size(147, 23);
            this.txtSnagaKW.TabIndex = 11;
            this.txtSnagaKW.UseSelectable = true;
            this.txtSnagaKW.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSnagaKW.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(22, 249);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(131, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Snaga motora u KW:";
            // 
            // txtGodiste
            // 
            // 
            // 
            // 
            this.txtGodiste.CustomButton.Image = null;
            this.txtGodiste.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtGodiste.CustomButton.Name = "";
            this.txtGodiste.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtGodiste.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtGodiste.CustomButton.TabIndex = 1;
            this.txtGodiste.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtGodiste.CustomButton.UseSelectable = true;
            this.txtGodiste.CustomButton.Visible = false;
            this.txtGodiste.Lines = new string[0];
            this.txtGodiste.Location = new System.Drawing.Point(159, 216);
            this.txtGodiste.MaxLength = 32767;
            this.txtGodiste.Name = "txtGodiste";
            this.txtGodiste.PasswordChar = '\0';
            this.txtGodiste.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGodiste.SelectedText = "";
            this.txtGodiste.SelectionLength = 0;
            this.txtGodiste.SelectionStart = 0;
            this.txtGodiste.ShortcutsEnabled = true;
            this.txtGodiste.Size = new System.Drawing.Size(147, 23);
            this.txtGodiste.TabIndex = 9;
            this.txtGodiste.UseSelectable = true;
            this.txtGodiste.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtGodiste.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(97, 220);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(56, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Godiste:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(213, 411);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(93, 41);
            this.btnDodaj.TabIndex = 19;
            this.btnDodaj.Text = "Dodaj vozilo";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(115, 191);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(38, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Boja:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(103, 162);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(50, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Model:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(44, 130);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(109, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "ID vlasnika vozila:";
            // 
            // txtBoja
            // 
            // 
            // 
            // 
            this.txtBoja.CustomButton.Image = null;
            this.txtBoja.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtBoja.CustomButton.Name = "";
            this.txtBoja.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoja.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoja.CustomButton.TabIndex = 1;
            this.txtBoja.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoja.CustomButton.UseSelectable = true;
            this.txtBoja.CustomButton.Visible = false;
            this.txtBoja.Lines = new string[0];
            this.txtBoja.Location = new System.Drawing.Point(159, 187);
            this.txtBoja.MaxLength = 32767;
            this.txtBoja.Name = "txtBoja";
            this.txtBoja.PasswordChar = '\0';
            this.txtBoja.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoja.SelectedText = "";
            this.txtBoja.SelectionLength = 0;
            this.txtBoja.SelectionStart = 0;
            this.txtBoja.ShortcutsEnabled = true;
            this.txtBoja.Size = new System.Drawing.Size(147, 23);
            this.txtBoja.TabIndex = 7;
            this.txtBoja.UseSelectable = true;
            this.txtBoja.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoja.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtModel
            // 
            // 
            // 
            // 
            this.txtModel.CustomButton.Image = null;
            this.txtModel.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtModel.CustomButton.Name = "";
            this.txtModel.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtModel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtModel.CustomButton.TabIndex = 1;
            this.txtModel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtModel.CustomButton.UseSelectable = true;
            this.txtModel.CustomButton.Visible = false;
            this.txtModel.Lines = new string[0];
            this.txtModel.Location = new System.Drawing.Point(159, 158);
            this.txtModel.MaxLength = 32767;
            this.txtModel.Name = "txtModel";
            this.txtModel.PasswordChar = '\0';
            this.txtModel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtModel.SelectedText = "";
            this.txtModel.SelectionLength = 0;
            this.txtModel.SelectionStart = 0;
            this.txtModel.ShortcutsEnabled = true;
            this.txtModel.Size = new System.Drawing.Size(147, 23);
            this.txtModel.TabIndex = 5;
            this.txtModel.UseSelectable = true;
            this.txtModel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtModel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtID
            // 
            // 
            // 
            // 
            this.txtID.CustomButton.Image = null;
            this.txtID.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtID.CustomButton.Name = "";
            this.txtID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtID.CustomButton.TabIndex = 1;
            this.txtID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtID.CustomButton.UseSelectable = true;
            this.txtID.CustomButton.Visible = false;
            this.txtID.Lines = new string[0];
            this.txtID.Location = new System.Drawing.Point(159, 126);
            this.txtID.MaxLength = 32767;
            this.txtID.Name = "txtID";
            this.txtID.PasswordChar = '\0';
            this.txtID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtID.SelectedText = "";
            this.txtID.SelectionLength = 0;
            this.txtID.SelectionStart = 0;
            this.txtID.ShortcutsEnabled = true;
            this.txtID.Size = new System.Drawing.Size(147, 23);
            this.txtID.TabIndex = 3;
            this.txtID.UseSelectable = true;
            this.txtID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtIDVozila
            // 
            // 
            // 
            // 
            this.txtIDVozila.CustomButton.Image = null;
            this.txtIDVozila.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.txtIDVozila.CustomButton.Name = "";
            this.txtIDVozila.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIDVozila.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIDVozila.CustomButton.TabIndex = 1;
            this.txtIDVozila.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIDVozila.CustomButton.UseSelectable = true;
            this.txtIDVozila.CustomButton.Visible = false;
            this.txtIDVozila.Lines = new string[0];
            this.txtIDVozila.Location = new System.Drawing.Point(159, 77);
            this.txtIDVozila.MaxLength = 32767;
            this.txtIDVozila.Name = "txtIDVozila";
            this.txtIDVozila.PasswordChar = '\0';
            this.txtIDVozila.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDVozila.SelectedText = "";
            this.txtIDVozila.SelectionLength = 0;
            this.txtIDVozila.SelectionStart = 0;
            this.txtIDVozila.ShortcutsEnabled = true;
            this.txtIDVozila.Size = new System.Drawing.Size(147, 23);
            this.txtIDVozila.TabIndex = 1;
            this.txtIDVozila.UseSelectable = true;
            this.txtIDVozila.Visible = false;
            this.txtIDVozila.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIDVozila.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtIDVozila.Leave += new System.EventHandler(this.txtIDVozila_Leave);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(92, 81);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(61, 19);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID vozila:";
            this.lblID.Visible = false;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(44, 411);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(94, 41);
            this.btnObrisi.TabIndex = 18;
            this.btnObrisi.Text = "Obrisi vozilo";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Visible = false;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // cbReg
            // 
            this.cbReg.AutoSize = true;
            this.cbReg.Location = new System.Drawing.Point(160, 333);
            this.cbReg.Name = "cbReg";
            this.cbReg.Size = new System.Drawing.Size(92, 15);
            this.cbReg.TabIndex = 16;
            this.cbReg.Text = "Registrovano";
            this.cbReg.UseSelectable = true;
            // 
            // cbOsig
            // 
            this.cbOsig.AutoSize = true;
            this.cbOsig.Location = new System.Drawing.Point(160, 354);
            this.cbOsig.Name = "cbOsig";
            this.cbOsig.Size = new System.Drawing.Size(78, 15);
            this.cbOsig.TabIndex = 17;
            this.cbOsig.Text = "Osigurano";
            this.cbOsig.UseSelectable = true;
            // 
            // FormVozilo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 475);
            this.Controls.Add(this.cbOsig);
            this.Controls.Add(this.cbReg);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.txtIDVozila);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.cmbVrstaVozila);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.txtZapremina);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.txtSnagaKW);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtGodiste);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtBoja);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.txtID);
            this.Name = "FormVozilo";
            this.Text = "Vozilo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormVozilo_FormClosing);
            this.Load += new System.EventHandler(this.FormVozilo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbVrstaVozila;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtZapremina;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtSnagaKW;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtGodiste;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtBoja;
        private MetroFramework.Controls.MetroTextBox txtModel;
        private MetroFramework.Controls.MetroTextBox txtID;
        private MetroFramework.Controls.MetroTextBox txtIDVozila;
        private MetroFramework.Controls.MetroLabel lblID;
        private MetroFramework.Controls.MetroButton btnObrisi;
        private MetroFramework.Controls.MetroCheckBox cbReg;
        private MetroFramework.Controls.MetroCheckBox cbOsig;
    }
}