﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public partial class FormVozilo : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;
        bool izmeni = false;
        ObjectId idZaModifikaciju;

        public FormVozilo()
        {
            InitializeComponent();
        }

        public FormVozilo(bool izmena, ObjectId id)
        {
            izmeni = izmena;
            idZaModifikaciju = id;
            InitializeComponent();
        }

        private void FormVozilo_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();

            if (izmeni)
            {
                btnDodaj.Text = "Izmeni";
                btnObrisi.Visible = true;
                lblID.Visible = true;
                txtIDVozila.Visible = true;
                PopuniKontrole(fb.vratiVoziloSaIDem(idZaModifikaciju));
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (!izmeni)
            {
                if (txtID.Text != "" && txtModel.Text != "" && txtBoja.Text != "" && txtGodiste.Text != "" &&
                    txtSnagaKW.Text != "" && txtZapremina.Text != "")
                {
                    ObjectId id = fb.dodajVozilo(ObjectId.Parse(txtID.Text), txtModel.Text, txtBoja.Text,
                        Int32.Parse(txtGodiste.Text), Int32.Parse(txtSnagaKW.Text),
                        float.Parse(txtZapremina.Text), cmbVrstaVozila.SelectedIndex,
                        cbReg.Checked, cbOsig.Checked);

                    if (id != ObjectId.Empty)
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno dodato vozilo!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        OcistiKontrole();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                    MetroFramework.MetroMessageBox.Show(this, "Sva polja moraju biti popunjena!", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (txtIDVozila.Text != "" && txtID.Text != "" && txtModel.Text != "" && txtBoja.Text != "" 
                    && txtGodiste.Text != "" && txtSnagaKW.Text != "" && txtZapremina.Text != "")
                {
                    if (fb.izmeniVozilo(ObjectId.Parse(txtIDVozila.Text), ObjectId.Parse(txtID.Text), 
                        txtModel.Text, txtBoja.Text, Int32.Parse(txtGodiste.Text), 
                        Int32.Parse(txtSnagaKW.Text), float.Parse(txtZapremina.Text), 
                        cmbVrstaVozila.SelectedIndex, cbReg.Checked, cbOsig.Checked))
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno izmenjeno vozilo!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                    MetroFramework.MetroMessageBox.Show(this, "Sva polja moraju biti popunjena!", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (txtIDVozila.Text != "")
            {
                if(fb.obrisiVozilo(ObjectId.Parse(txtIDVozila.Text)))
                {
                    MetroFramework.MetroMessageBox.Show(this, "Uspesno obrisano vozilo!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Morate uneti ID!", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtIDVozila_Leave(object sender, EventArgs e)
        {
            ObjectId id;
            if(ObjectId.TryParse(txtIDVozila.Text, out id))
            {
                Vozilo v = fb.vratiVoziloSaIDem(id);
                PopuniKontrole(v);
            }
        }

        public void PopuniKontrole(Vozilo v)
        {
            txtIDVozila.Text = v.Id.ToString();
            txtID.Text = v.IdVlasnika.ToString();
            txtModel.Text = v.Model;
            txtBoja.Text = v.Boja;
            txtGodiste.Text = v.Godiste.ToString();
            txtSnagaKW.Text = v.SnagaKW.ToString();
            txtZapremina.Text = v.Zapremina.ToString();
            cmbVrstaVozila.SelectedIndex = v.VrstaVozila;
            cbReg.Checked = v.Registrovano;
            cbOsig.Checked = v.Osigurano;
        }

        public void OcistiKontrole()
        {
            txtIDVozila.Text = "";
            txtID.Text = "";
            txtModel.Text = "";
            txtBoja.Text = "";
            txtGodiste.Text = "";
            txtSnagaKW.Text = "";
            txtZapremina.Text = "";
            cmbVrstaVozila.SelectedIndex = 0;
            cbReg.Checked = false;
            cbOsig.Checked = false;
        }

        private void FormVozilo_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
