﻿namespace MongoDB_Repository_Servis
{
    partial class FormLoginServis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtIDservisa = new MetroFramework.Controls.MetroTextBox();
            this.btnLogin = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(62, 88);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(113, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Unesite ID servisa:";
            // 
            // txtIDservisa
            // 
            // 
            // 
            // 
            this.txtIDservisa.CustomButton.Image = null;
            this.txtIDservisa.CustomButton.Location = new System.Drawing.Point(135, 1);
            this.txtIDservisa.CustomButton.Name = "";
            this.txtIDservisa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIDservisa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIDservisa.CustomButton.TabIndex = 1;
            this.txtIDservisa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIDservisa.CustomButton.UseSelectable = true;
            this.txtIDservisa.CustomButton.Visible = false;
            this.txtIDservisa.Lines = new string[0];
            this.txtIDservisa.Location = new System.Drawing.Point(39, 110);
            this.txtIDservisa.MaxLength = 32767;
            this.txtIDservisa.Name = "txtIDservisa";
            this.txtIDservisa.PasswordChar = '\0';
            this.txtIDservisa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDservisa.SelectedText = "";
            this.txtIDservisa.SelectionLength = 0;
            this.txtIDservisa.SelectionStart = 0;
            this.txtIDservisa.ShortcutsEnabled = true;
            this.txtIDservisa.Size = new System.Drawing.Size(157, 23);
            this.txtIDservisa.TabIndex = 1;
            this.txtIDservisa.UseSelectable = true;
            this.txtIDservisa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIDservisa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(77, 169);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "Prijavite se";
            this.btnLogin.UseSelectable = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // FormLoginServis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 224);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtIDservisa);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FormLoginServis";
            this.Text = "Prijava";
            this.Load += new System.EventHandler(this.FormLoginServis_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtIDservisa;
        private MetroFramework.Controls.MetroButton btnLogin;
    }
}