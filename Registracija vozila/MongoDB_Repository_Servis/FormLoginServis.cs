﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDB_Repository_Servis
{
    public partial class FormLoginServis : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;

        public FormLoginServis()
        {
            InitializeComponent();
        }

        private void FormLoginServis_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtIDservisa.Text != "")
            {
                ServisTehnickogPregleda s = fb.loginServisa(ObjectId.Parse(txtIDservisa.Text));
                if (s == null)
                {
                    MetroFramework.MetroMessageBox.Show(this, "Servis ne postoji!", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    FormServis servis = new FormServis(s);
                    servis.ShowDialog();
                }
            }
            else
                MetroFramework.MetroMessageBox.Show(this, "Unesite ID Vaseg servisa!", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
    }
}
