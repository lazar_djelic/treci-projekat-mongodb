﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public partial class FormTehnickiPregled : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;
        ObjectId idZaTP;

        public FormTehnickiPregled()
        {
            InitializeComponent();
        }

        public FormTehnickiPregled(ObjectId id)
        {
            idZaTP = id;
            InitializeComponent();
        }

        private void FormTehnickiPregled_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();

            if (idZaTP != ObjectId.Empty)
                txtIDvozila.Text = idZaTP.ToString();
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            if (txtIDvozila.Text != "")
            {
                String idVozila = txtIDvozila.Text;
                TehnickiPregled t_Pregled = new TehnickiPregled(ObjectId.Parse(idVozila), DateTime.Now.ToString(), clbPregled.CheckedItems.Count);

                if (fb.dodajTehnickiPregled(t_Pregled))
                {
                    MetroFramework.MetroMessageBox.Show(this, "Tehnicki pregled uspesno dodat!", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MetroFramework.MetroMessageBox.Show(this, "Morate uneti ID vozila!", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void FormTehnickiPregled_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
