﻿namespace MongoDB_Repository_Servis
{
    partial class FormVozac
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtID = new MetroFramework.Controls.MetroTextBox();
            this.lblID = new MetroFramework.Controls.MetroLabel();
            this.btnDodajVozaca = new MetroFramework.Controls.MetroButton();
            this.txtBTelefona = new MetroFramework.Controls.MetroTextBox();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.txtBLK = new MetroFramework.Controls.MetroTextBox();
            this.txtJMBG = new MetroFramework.Controls.MetroTextBox();
            this.txtIme = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnObrisi = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtID
            // 
            // 
            // 
            // 
            this.txtID.CustomButton.Image = null;
            this.txtID.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtID.CustomButton.Name = "";
            this.txtID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtID.CustomButton.TabIndex = 1;
            this.txtID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtID.CustomButton.UseSelectable = true;
            this.txtID.CustomButton.Visible = false;
            this.txtID.Lines = new string[0];
            this.txtID.Location = new System.Drawing.Point(124, 63);
            this.txtID.MaxLength = 32767;
            this.txtID.Name = "txtID";
            this.txtID.PasswordChar = '\0';
            this.txtID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtID.SelectedText = "";
            this.txtID.SelectionLength = 0;
            this.txtID.SelectionStart = 0;
            this.txtID.ShortcutsEnabled = true;
            this.txtID.Size = new System.Drawing.Size(161, 23);
            this.txtID.TabIndex = 1;
            this.txtID.UseSelectable = true;
            this.txtID.Visible = false;
            this.txtID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtID.Leave += new System.EventHandler(this.txtID_Leave);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(39, 67);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(68, 19);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID vozaca:";
            this.lblID.Visible = false;
            // 
            // btnDodajVozaca
            // 
            this.btnDodajVozaca.Location = new System.Drawing.Point(188, 331);
            this.btnDodajVozaca.Name = "btnDodajVozaca";
            this.btnDodajVozaca.Size = new System.Drawing.Size(97, 39);
            this.btnDodajVozaca.TabIndex = 15;
            this.btnDodajVozaca.Text = "Dodaj vozaca";
            this.btnDodajVozaca.UseSelectable = true;
            this.btnDodajVozaca.Click += new System.EventHandler(this.btnDodajVozaca_Click);
            // 
            // txtBTelefona
            // 
            // 
            // 
            // 
            this.txtBTelefona.CustomButton.Image = null;
            this.txtBTelefona.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtBTelefona.CustomButton.Name = "";
            this.txtBTelefona.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBTelefona.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBTelefona.CustomButton.TabIndex = 1;
            this.txtBTelefona.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBTelefona.CustomButton.UseSelectable = true;
            this.txtBTelefona.CustomButton.Visible = false;
            this.txtBTelefona.Lines = new string[0];
            this.txtBTelefona.Location = new System.Drawing.Point(124, 255);
            this.txtBTelefona.MaxLength = 32767;
            this.txtBTelefona.Name = "txtBTelefona";
            this.txtBTelefona.PasswordChar = '\0';
            this.txtBTelefona.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBTelefona.SelectedText = "";
            this.txtBTelefona.SelectionLength = 0;
            this.txtBTelefona.SelectionStart = 0;
            this.txtBTelefona.ShortcutsEnabled = true;
            this.txtBTelefona.Size = new System.Drawing.Size(161, 23);
            this.txtBTelefona.TabIndex = 13;
            this.txtBTelefona.UseSelectable = true;
            this.txtBTelefona.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBTelefona.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(124, 136);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(161, 23);
            this.txtPrezime.TabIndex = 5;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(124, 226);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(161, 23);
            this.txtAdresa.TabIndex = 11;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtBLK
            // 
            // 
            // 
            // 
            this.txtBLK.CustomButton.Image = null;
            this.txtBLK.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtBLK.CustomButton.Name = "";
            this.txtBLK.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBLK.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBLK.CustomButton.TabIndex = 1;
            this.txtBLK.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBLK.CustomButton.UseSelectable = true;
            this.txtBLK.CustomButton.Visible = false;
            this.txtBLK.Lines = new string[0];
            this.txtBLK.Location = new System.Drawing.Point(124, 197);
            this.txtBLK.MaxLength = 32767;
            this.txtBLK.Name = "txtBLK";
            this.txtBLK.PasswordChar = '\0';
            this.txtBLK.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBLK.SelectedText = "";
            this.txtBLK.SelectionLength = 0;
            this.txtBLK.SelectionStart = 0;
            this.txtBLK.ShortcutsEnabled = true;
            this.txtBLK.Size = new System.Drawing.Size(161, 23);
            this.txtBLK.TabIndex = 9;
            this.txtBLK.UseSelectable = true;
            this.txtBLK.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBLK.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtJMBG
            // 
            // 
            // 
            // 
            this.txtJMBG.CustomButton.Image = null;
            this.txtJMBG.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtJMBG.CustomButton.Name = "";
            this.txtJMBG.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtJMBG.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtJMBG.CustomButton.TabIndex = 1;
            this.txtJMBG.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtJMBG.CustomButton.UseSelectable = true;
            this.txtJMBG.CustomButton.Visible = false;
            this.txtJMBG.Lines = new string[0];
            this.txtJMBG.Location = new System.Drawing.Point(124, 165);
            this.txtJMBG.MaxLength = 32767;
            this.txtJMBG.Name = "txtJMBG";
            this.txtJMBG.PasswordChar = '\0';
            this.txtJMBG.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtJMBG.SelectedText = "";
            this.txtJMBG.SelectionLength = 0;
            this.txtJMBG.SelectionStart = 0;
            this.txtJMBG.ShortcutsEnabled = true;
            this.txtJMBG.Size = new System.Drawing.Size(161, 23);
            this.txtJMBG.TabIndex = 7;
            this.txtJMBG.UseSelectable = true;
            this.txtJMBG.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtJMBG.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtIme
            // 
            // 
            // 
            // 
            this.txtIme.CustomButton.Image = null;
            this.txtIme.CustomButton.Location = new System.Drawing.Point(139, 1);
            this.txtIme.CustomButton.Name = "";
            this.txtIme.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIme.CustomButton.TabIndex = 1;
            this.txtIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIme.CustomButton.UseSelectable = true;
            this.txtIme.CustomButton.Visible = false;
            this.txtIme.Lines = new string[0];
            this.txtIme.Location = new System.Drawing.Point(124, 107);
            this.txtIme.MaxLength = 32767;
            this.txtIme.Name = "txtIme";
            this.txtIme.PasswordChar = '\0';
            this.txtIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIme.SelectedText = "";
            this.txtIme.SelectionLength = 0;
            this.txtIme.SelectionStart = 0;
            this.txtIme.ShortcutsEnabled = true;
            this.txtIme.Size = new System.Drawing.Size(161, 23);
            this.txtIme.TabIndex = 3;
            this.txtIme.UseSelectable = true;
            this.txtIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(20, 259);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(87, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Broj telefona:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(54, 230);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(53, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Adresa:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(8, 201);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(100, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Broj licne karte:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(61, 169);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(46, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "JMBG:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(49, 140);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(60, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Prezime:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(73, 111);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(34, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Ime:";
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(39, 331);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(105, 39);
            this.btnObrisi.TabIndex = 14;
            this.btnObrisi.Text = "Obrisi vozaca";
            this.btnObrisi.UseSelectable = true;
            this.btnObrisi.Visible = false;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // FormVozac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 410);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.btnDodajVozaca);
            this.Controls.Add(this.txtBTelefona);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.txtBLK);
            this.Controls.Add(this.txtJMBG);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FormVozac";
            this.Text = "Vozac";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormVozac_FormClosing);
            this.Load += new System.EventHandler(this.FormVozac_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtID;
        private MetroFramework.Controls.MetroLabel lblID;
        private MetroFramework.Controls.MetroButton btnDodajVozaca;
        private MetroFramework.Controls.MetroTextBox txtBTelefona;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtAdresa;
        private MetroFramework.Controls.MetroTextBox txtBLK;
        private MetroFramework.Controls.MetroTextBox txtJMBG;
        private MetroFramework.Controls.MetroTextBox txtIme;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnObrisi;
    }
}