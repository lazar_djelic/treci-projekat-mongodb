﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public class Vozilo
    {
        public ObjectId Id { get; set; }
        public string Model { get; set; }
        public string Boja { get; set; }
        public int Godiste { get; set; }
        public int SnagaKW { get; set; }
        public float Zapremina { get; set; }
        public int VrstaVozila { get; set; } //Putnicko vozilo = 0, Teretno = 1, Motocikl = 2, Autobus = 3
        public ObjectId IdVlasnika { get; set; }
        public bool Registrovano { get; set; }
        public bool Osigurano { get; set; }
    }
}
