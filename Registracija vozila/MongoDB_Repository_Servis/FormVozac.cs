﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public partial class FormVozac : MetroFramework.Forms.MetroForm
    {
        FunkcijeBaze fb;
        bool izmeni = false;
        ObjectId idZaIzmenu;

        public FormVozac()
        {
            InitializeComponent();
        }

        public FormVozac(bool izmena, ObjectId ajdi)
        {
            izmeni = izmena;
            idZaIzmenu = ajdi;
            InitializeComponent();
        }

        private void FormVozac_Load(object sender, EventArgs e)
        {
            fb = new FunkcijeBaze();

            if (izmeni)
            {
                btnDodajVozaca.Text = "Izmeni";
                btnObrisi.Visible = true;
                lblID.Visible = true;
                txtID.Visible = true;
                PopuniKontrole(fb.vratiVozacaSaIDem(idZaIzmenu));
            }
        }

        private void btnDodajVozaca_Click(object sender, EventArgs e)
        {
            if (!izmeni)
            {
                if (txtIme.Text != "" && txtPrezime.Text != "" && txtJMBG.Text != "" &&
                        txtBLK.Text != "" && txtBTelefona.Text != "" && txtAdresa.Text != "")
                {
                    ObjectId id = fb.dodajVozaca(txtIme.Text, txtPrezime.Text, txtJMBG.Text, txtBLK.Text, txtAdresa.Text, txtBTelefona.Text);

                    if (id != ObjectId.Empty)
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno dodat vozac!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        OcistiKontrole();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MetroFramework.MetroMessageBox.Show(this, "Sva polja moraju biti popunjena!", "",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (txtIme.Text != "" && txtPrezime.Text != "" && txtJMBG.Text != "" && txtID.Text != "" &&
                        txtBLK.Text != "" && txtBTelefona.Text != "" && txtAdresa.Text != "")
                {
                    if (fb.izmeniVozaca(ObjectId.Parse(txtID.Text), txtIme.Text, txtPrezime.Text, txtJMBG.Text, txtBLK.Text, txtAdresa.Text, txtBTelefona.Text))
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Uspesno izmenjen vozac!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MetroFramework.MetroMessageBox.Show(this, "Sva polja moraju biti popunjena!", "",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (txtID.Text != "")
            {
                if (fb.obrisiVozaca(ObjectId.Parse(txtID.Text)))
                {
                    MetroFramework.MetroMessageBox.Show(this, "Uspesno obrisan vozac!",
                            "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MetroFramework.MetroMessageBox.Show(this, "Doslo je do greske!", "",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Morate uneti ID!", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtID_Leave(object sender, EventArgs e)
        {
            ObjectId id;
            if (ObjectId.TryParse(txtID.Text, out id))
            {
                Vozac v = fb.vratiVozacaSaIDem(id);
                PopuniKontrole(v);
            }
        }

        public void PopuniKontrole(Vozac v)
        {
            txtID.Text = v.Id.ToString();
            txtIme.Text = v.Ime;
            txtPrezime.Text = v.Prezime;
            txtJMBG.Text = v.JMBG;
            txtBLK.Text = v.BrLicneKarte;
            txtAdresa.Text = v.Adresa;
            txtBTelefona.Text = v.Telefon;
        }

        public void OcistiKontrole()
        {
            txtID.Text = "";
            txtIme.Text = "";
            txtPrezime.Text = "";
            txtJMBG.Text = "";
            txtBLK.Text = "";
            txtAdresa.Text = "";
            txtBTelefona.Text = "";
        }

        private void FormVozac_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
