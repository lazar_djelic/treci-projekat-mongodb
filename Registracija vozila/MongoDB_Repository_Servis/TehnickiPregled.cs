﻿using MongoDB.Bson;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB_Repository_Servis
{
    class TehnickiPregled
    {
        public ObjectId Id { get; set; }
        public ObjectId IdVozila { get; set; }
        public String Datum { get; set; }
        public Int32 IspunjenoUslova { get; set; }

        public TehnickiPregled(ObjectId idVozila, string datum, Int32 ispUsl)
        {
            IdVozila = idVozila;
            Datum = datum;
            IspunjenoUslova = ispUsl;
        }
    }
}
