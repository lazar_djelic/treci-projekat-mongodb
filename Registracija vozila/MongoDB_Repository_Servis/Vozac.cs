﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;

namespace MongoDB_Repository_Servis
{
    public class Vozac
    {
        public ObjectId Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string JMBG { get; set; }
        public string BrLicneKarte { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
    }
}
