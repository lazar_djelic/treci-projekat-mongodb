﻿namespace MongoDB_Repository_Servis
{
    partial class FormServis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaziv = new MetroFramework.Controls.MetroLabel();
            this.btnRegistruj = new MetroFramework.Controls.MetroButton();
            this.btnTPregled = new MetroFramework.Controls.MetroButton();
            this.btnVozilo = new MetroFramework.Controls.MetroButton();
            this.btnVozac = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblObavestenje1 = new MetroFramework.Controls.MetroLabel();
            this.dgvVozaci = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblObavestenje2 = new MetroFramework.Controls.MetroLabel();
            this.dgvVozila = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVozaci)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVozila)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Location = new System.Drawing.Point(23, 60);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(84, 19);
            this.lblNaziv.TabIndex = 0;
            this.lblNaziv.Text = "Naziv servisa";
            this.lblNaziv.Visible = false;
            // 
            // btnRegistruj
            // 
            this.btnRegistruj.Location = new System.Drawing.Point(637, 29);
            this.btnRegistruj.Name = "btnRegistruj";
            this.btnRegistruj.Size = new System.Drawing.Size(86, 37);
            this.btnRegistruj.TabIndex = 2;
            this.btnRegistruj.Text = "Registruj \r\nvozilo";
            this.btnRegistruj.UseSelectable = true;
            this.btnRegistruj.Click += new System.EventHandler(this.btnRegistruj_Click);
            // 
            // btnTPregled
            // 
            this.btnTPregled.Location = new System.Drawing.Point(498, 29);
            this.btnTPregled.Name = "btnTPregled";
            this.btnTPregled.Size = new System.Drawing.Size(133, 37);
            this.btnTPregled.TabIndex = 1;
            this.btnTPregled.Text = "Obavi tehnicki pregled";
            this.btnTPregled.UseSelectable = true;
            this.btnTPregled.Click += new System.EventHandler(this.btnTPregled_Click);
            // 
            // btnVozilo
            // 
            this.btnVozilo.Location = new System.Drawing.Point(6, 29);
            this.btnVozilo.Name = "btnVozilo";
            this.btnVozilo.Size = new System.Drawing.Size(133, 37);
            this.btnVozilo.TabIndex = 0;
            this.btnVozilo.Text = "Upravljaj vozilima";
            this.btnVozilo.UseSelectable = true;
            this.btnVozilo.Click += new System.EventHandler(this.btnVozilo_Click);
            // 
            // btnVozac
            // 
            this.btnVozac.Location = new System.Drawing.Point(6, 31);
            this.btnVozac.Name = "btnVozac";
            this.btnVozac.Size = new System.Drawing.Size(133, 37);
            this.btnVozac.TabIndex = 0;
            this.btnVozac.Text = "Upravljaj vozacima";
            this.btnVozac.UseSelectable = true;
            this.btnVozac.Click += new System.EventHandler(this.btnVozac_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblObavestenje1);
            this.groupBox1.Controls.Add(this.dgvVozaci);
            this.groupBox1.Controls.Add(this.btnVozac);
            this.groupBox1.Location = new System.Drawing.Point(23, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(729, 284);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vozaci";
            // 
            // lblObavestenje1
            // 
            this.lblObavestenje1.AutoSize = true;
            this.lblObavestenje1.Location = new System.Drawing.Point(7, 73);
            this.lblObavestenje1.Name = "lblObavestenje1";
            this.lblObavestenje1.Size = new System.Drawing.Size(0, 0);
            this.lblObavestenje1.TabIndex = 1;
            // 
            // dgvVozaci
            // 
            this.dgvVozaci.AllowUserToAddRows = false;
            this.dgvVozaci.AllowUserToDeleteRows = false;
            this.dgvVozaci.AllowUserToResizeRows = false;
            this.dgvVozaci.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvVozaci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVozaci.Location = new System.Drawing.Point(7, 95);
            this.dgvVozaci.MultiSelect = false;
            this.dgvVozaci.Name = "dgvVozaci";
            this.dgvVozaci.ReadOnly = true;
            this.dgvVozaci.Size = new System.Drawing.Size(716, 183);
            this.dgvVozaci.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblObavestenje2);
            this.groupBox2.Controls.Add(this.dgvVozila);
            this.groupBox2.Controls.Add(this.btnRegistruj);
            this.groupBox2.Controls.Add(this.btnTPregled);
            this.groupBox2.Controls.Add(this.btnVozilo);
            this.groupBox2.Location = new System.Drawing.Point(23, 393);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(729, 283);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vozila";
            // 
            // lblObavestenje2
            // 
            this.lblObavestenje2.AutoSize = true;
            this.lblObavestenje2.Location = new System.Drawing.Point(7, 69);
            this.lblObavestenje2.Name = "lblObavestenje2";
            this.lblObavestenje2.Size = new System.Drawing.Size(0, 0);
            this.lblObavestenje2.TabIndex = 3;
            // 
            // dgvVozila
            // 
            this.dgvVozila.AllowUserToAddRows = false;
            this.dgvVozila.AllowUserToDeleteRows = false;
            this.dgvVozila.AllowUserToResizeRows = false;
            this.dgvVozila.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvVozila.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVozila.Location = new System.Drawing.Point(7, 91);
            this.dgvVozila.MultiSelect = false;
            this.dgvVozila.Name = "dgvVozila";
            this.dgvVozila.ReadOnly = true;
            this.dgvVozila.Size = new System.Drawing.Size(716, 186);
            this.dgvVozila.TabIndex = 4;
            // 
            // FormServis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 704);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblNaziv);
            this.Name = "FormServis";
            this.Text = "Servis";
            this.Load += new System.EventHandler(this.FormServis_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVozaci)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVozila)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblNaziv;
        private MetroFramework.Controls.MetroButton btnRegistruj;
        private MetroFramework.Controls.MetroButton btnTPregled;
        private MetroFramework.Controls.MetroButton btnVozilo;
        private MetroFramework.Controls.MetroButton btnVozac;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvVozaci;
        private System.Windows.Forms.DataGridView dgvVozila;
        private MetroFramework.Controls.MetroLabel lblObavestenje1;
        private MetroFramework.Controls.MetroLabel lblObavestenje2;
    }
}